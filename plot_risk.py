#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 16:18:43 2020

@author: pavelvazquez
"""
import pandas as pd
import pickle
from matplotlib import pyplot as plt


dataf = pd.read_csv("ETH_elderly_60_plus_2019-07-06.csv")

distances = pickle.load( open( "distances.pkl", "rb" ) )

dataf.loc[:,'distance'] = distances


fig = plt.figure(figsize=(16,16))
plt.grid('off')
plt.axis('off')
fig.axes[0].get_xaxis().set_visible(False)
fig.axes[0].get_yaxis().set_visible(False)
plt.tight_layout()

color=(dataf["distance"]-dataf["distance"].min())/(dataf["distance"].max()-dataf["distance"].min())
#color = [(item/255.) for item in dataf["distance"]]
plt.scatter(dataf["longitude"],dataf["latitude"], c=color, s=0.1, cmap='Reds')
#plt.show()
plt.savefig('ethiopia_risk.png',dpi = 600)