#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 13:03:17 2020

@author: pavelvazquez
"""
import pandas as pd
import copy
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm


def density_map(latitudes, longitudes,x,y, center, bins=1000, radius=8):  
    cmap = copy.copy(plt.cm.jet)
    cmap.set_bad((0,0,0))  # Fill background with black

    # Center the map around the provided center coordinates
    histogram_range = [
        [center[1] - radius, center[1] + radius],
        [center[0] - radius, center[0] + radius]
    ]
    
    fig = plt.figure(figsize=(16,16))
    plt.hist2d(longitudes, latitudes, bins=bins, norm=LogNorm(),
               cmap=plt.cm.YlOrRd, range=histogram_range)
    plt.scatter(x, y, 20, marker='o', color='Blue') 
    # Remove all axes and annotations to keep the map clean and simple
    plt.grid('off')
    plt.axis('off')
    fig.axes[0].get_xaxis().set_visible(False)
    fig.axes[0].get_yaxis().set_visible(False)
    plt.tight_layout()
    #plt.show()
    plt.savefig('ethiopia.png',dpi = 600, transparent=True)
    
    
datam = pd.read_csv("ethiopia.csv")
amenities = ['pharmacy','doctor', ]
datam = datam[~datam.amenity.isin(amenities)]

xm = datam['X'].tolist()
ym = datam['Y'].tolist()    
    
dataf = pd.read_csv("ETH_elderly_60_plus_2019-07-06.csv")
xf = dataf['longitude'].tolist()
yf = dataf['latitude'].tolist()
    
    
    
    
center_e = [8.349612, 39.914925]
# Separate the latitude and longitude values from our list of coordinates
latitudes = yf
longitudes = xf
# Render the map
density_map(latitudes, longitudes,xm,ym, center=center_e)
