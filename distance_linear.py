#!/usr/bin/env python3# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 15:30:45 2020

@author: pavelvazquez
"""
import pandas as pd
import csv
import pickle

datam = pd.read_csv("ethiopia.csv")
amenities = ['pharmacy','doctor', ]
datam = datam[~datam.amenity.isin(amenities)]
datam = datam[datam['X'].notna()]

dataf = pd.read_csv("ETH_elderly_60_plus_2019-07-06.csv")




from math import sin, cos, sqrt, atan2, radians

# approximate radius of earth in km
R = 6373.0


distances=[]
for i in range(0,20):
  dist=[]
  for j in range(0,len(datam)):

    lat1 = radians(dataf.iloc[i,0])
    lon1 = radians(dataf.iloc[i,1])
    lat2 = radians(datam.iloc[j,1])
    lon2 = radians(datam.iloc[j,0])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c
    dist.append(distance)
    
  distances.append(min(dist))
  print(i)

dataf = dataf.append(pd.DataFrame(distances), ignore_index=True)

with open('medical.pkl', 'wb') as f:
    pickle.dump(dataf, f)
with open('distances.pkl', 'wb') as f:
    pickle.dump(distances, f)