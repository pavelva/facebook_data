#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 11:42:36 2020

@author: pavelvazquez
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap





datam = pd.read_csv("ethiopia.csv")
amenities = ['pharmacy','doctor', ]
datam = datam[~datam.amenity.isin(amenities)]

xm = datam['X'].tolist()
ym = datam['Y'].tolist()

#dataf = pd.read_csv("ETH_elderly_60_plus_2019-07-06.csv")
xf = dataf['longitude'].tolist()
yf = dataf['latitude'].tolist()
c = dataf['population'].tolist()




#####################################
#lats = np.random.randint(-75, 75, size=20)
#lons = np.random.randint(-179, 179, size=20)

fig = plt.gcf()
fig.set_size_inches(8, 8)

m = Basemap(projection='merc', \
            llcrnrlat=3, urcrnrlat=15, \
            llcrnrlon=30, urcrnrlon=50, \
            lat_ts=0, \
            resolution='c')

m.bluemarble(scale=1.0)   # full scale will be overkill
m.drawcoastlines(color='white', linewidth=0.2)  # add coastlines
m.drawcountries(linewidth=2.5, linestyle='solid', color='k', antialiased=1, ax=None, zorder=None)
xm, ym = m(xm, ym)  # transform coordinates
xf, yf = m(xf, yf)  # transform coordinates
#plt.scatter(xm, ym, 20, marker='o', color='Red') 
plt.scatter(xf, yf, 0.1, marker='o', c=c, cmap=plt.cm.YlOrRd) 
plt.show()
          
#m = Basemap(projection='merc',
 #           lcrnrlat=0, urcrnrlat=15, 
  #          llcrnrlon=30, urcrnrlon=50, 
   #        lat_ts=20,
    #        resolution='c')





