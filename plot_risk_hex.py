#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 17:21:12 2020

@author: pavelvazquez
"""
import pandas as pd
import pickle

import numpy as np

dataf = pd.read_csv("ETH_elderly_60_plus_2019-07-06.csv")

distances = pickle.load( open( "distances.pkl", "rb" ) )

dataf.loc[:,'distance'] = distances



####
#Map
#plot = dataf.plot.hexbin(x='longitude',y='latitude',C='distance',reduce_C_function=np.sum, gridsize=100, cmap="Reds",figsize=(16,16),colorbar=False, legend=False)
#fig = plot.get_figure()
#fig.savefig("risk.png")

#histogram
import matplotlib.pyplot as plt


# matplotlib histogram
fig=plt.hist(dataf['distance'], color = 'blue', edgecolor = 'black',
         bins = int(180/4))

# seaborn histogram

# Add labels
#plt.title('Histogram')

plt.xlabel('Distance km')
plt.ylabel('# of users over 60 yrs')
plt.savefig('ethiopia_histogram.png',dpi = 400,bbox_inches = "tight")